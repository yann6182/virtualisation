import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateAccountComponent } from './create-account/create-account.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { InscriptionComponent } from './inscription/inscription.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ProfilComponent } from './profil/profil.component';
import { UserInfoComponent } from './user-info/user-info.component';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent, CreateAccountComponent, InscriptionComponent, ConnexionComponent, ForgotPasswordComponent, ResetPasswordComponent, ProfilComponent, UserInfoComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,HttpClientModule,FormsModule, NgbModule,ToastrModule.forRoot({
      timeOut: 5000, // Temps d'affichage global en millisecondes (5 secondes dans cet exemple)
      positionClass: 'toast-top', // Classe CSS globale pour positionner les toasts en haut
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
