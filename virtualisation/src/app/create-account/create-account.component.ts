import { Component } from '@angular/core';
import { AuthService } from '../auth-service.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss']
})
export class CreateAccountComponent {
  username: string |any;
  email: string |any;
  password: string |any;
  avatar: File |any;
message: string|any;

  constructor(private authService: AuthService,private http: HttpClient) {}

  createAccount() {
    const formData = new FormData();
    formData.append('username', this.username);
    formData.append('email', this.email);
    formData.append('password', this.password);
    formData.append('avatar', this.avatar);

    this.authService.createAccount(formData)
      .subscribe(
        response => {
          // Gérer la réponse du backend (par exemple, afficher un message de succès)
          console.log('Compte créé avec succès !');
        },
        error => {
          // Gérer les erreurs (par exemple, afficher un message d'erreur)
          console.error('Erreur lors de la création du compte :', error);
        }
      );
  }
  onFileSelected(event:any) {
    this.avatar = event.target.files[0];
  }

 testConnection() {
    this.http.get<any>('http://localhost:13000/api/test').subscribe((response: any) => {
      this.message = response.message;
    });
  }
}
