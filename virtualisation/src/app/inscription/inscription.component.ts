import { Component } from '@angular/core';
import { BackendService } from '../backend.service';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.scss']
})
export class InscriptionComponent {
  nom: string = '';
  email: string = '';
  motDePasse: string = '';
  image: File | undefined;

  constructor(private backendService: BackendService) {}

  inscription() {
    const formData = new FormData();
    formData.append('nom', this.nom);
    formData.append('email', this.email);
    formData.append('mot_de_passe', this.motDePasse);
    formData.append('image', this.image as Blob);

    console.log('Données du formulaire envoyées au backend :', formData);

    this.backendService.inscription(formData).subscribe(
      (response) => {
        console.log(response);
        // Rediriger l'utilisateur ou afficher un message de succès
      },
      (error) => {
        console.error(error);
        // Afficher un message d'erreur à l'utilisateur
      }
    );
  }

  onFileSelected(event: any) {
    this.image = event.target.files[0];
    console.log('Fichier sélectionné :', this.image);
  }
}
