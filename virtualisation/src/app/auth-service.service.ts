import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private s3Url = 'http://172.21.0.5:4566';
  private baseUrl = 'http://localhost:13000'; 

  constructor(private http: HttpClient) {}
  uploadFile(file: File) {
    const formData = new FormData();
    formData.append('file', file);
  
    return this.http.post(`${this.s3Url}/mon-bucket`, formData);
  }
  createAccount(formData: FormData) {
    return this.http.post<any>(`${this.baseUrl}/signup`, formData);
  }

  requestPasswordReset(email: string): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/forgot-password`, { email });
  }

  resetPassword(token: string, newPassword: string): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/reset-password`, { token, newPassword });
  }
}
