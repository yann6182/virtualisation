import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class BackendService {
  private apiUrl = 'http://localhost:13000'; 

  constructor(private http: HttpClient, private toastr: ToastrService) { }

  inscription(data: any): Observable<any> {
    return this.http.post(`${this.apiUrl}/inscription`, data);
  }

  connexion(data: any): Observable<any> {
    return this.http.post(`${this.apiUrl}/connexion`, data);
  }

  showUserInfoToast(user: any): void {
    const toastOptions = {
      closeButton: true,
      timeOut: 5000, // Durée d'affichage du toast (en millisecondes)
      extendedTimeOut: 2000, // Temps supplémentaire d'affichage du toast après survol (en millisecondes)
      progressBar: true, // Afficher une barre de progression
      tapToDismiss: false, // Empêcher la fermeture du toast en cliquant dessus
      positionClass: 'toast-top-center', // Position du toast en haut-centre
      toastClass: 'custom-toast', // Classe CSS personnalisée pour le toast
      titleClass: 'toast-title', // Classe CSS pour le titre du toast
      messageClass: 'toast-message' // Classe CSS pour le message du toast
    };

    this.toastr.success(`Bienvenue, ${user.nom} (${user.email})`, 'Informations Utilisateur', toastOptions);
  }
 
}
