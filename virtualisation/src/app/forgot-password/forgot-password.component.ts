import { Component } from '@angular/core';
import { AuthService } from '../auth-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent {
  email: string = '';

  constructor(private authService: AuthService, private router: Router) {}

  submitForm() {
    this.authService.requestPasswordReset(this.email)
      .subscribe(
        () => {
          alert('Un e-mail de réinitialisation de mot de passe a été envoyé.');
          // Rediriger l'utilisateur vers la page de réinitialisation du mot de passe
          this.router.navigate(['/reset-password']);
        },
        error => {
          console.error('Erreur lors de la demande de réinitialisation de mot de passe:', error);
          alert('Une erreur s\'est produite. Veuillez réessayer plus tard.');
        }
      );
  }
}
