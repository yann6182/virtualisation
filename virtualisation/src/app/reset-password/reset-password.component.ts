import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth-service.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  token: string = '';
  newPassword: string = '';

  constructor(private authService: AuthService,private route: ActivatedRoute) {}

   submitResetPasswordForm() {
    this.authService.resetPassword(this.token, this.newPassword)
      .subscribe(
        () => {
          alert('Le mot de passe a été réinitialisé avec succès.');
          // Rediriger l'utilisateur vers une autre page ou afficher un message de succès
        },
        error => {
          console.error('Erreur lors de la réinitialisation du mot de passe:', error);
          if (error.status === 404) {
            alert('Token invalide.');
          } else {
            alert('Une erreur s\'est produite. Veuillez réessayer plus tard.');
          }
        }
      );
  }


 ngOnInit(): void {
  this.route.paramMap.subscribe(params => {
    const token = params.get('token');
    if (token !== null) {
      this.token = token;
    } else {
      // Handle the case when token is null
    }
  });
}

}
