import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BackendService } from '../backend.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.scss']
})
export class ConnexionComponent {
  email: string = '';
  motDePasse: string = '';
  user: any;

  constructor(private router: Router, private backendService: BackendService, private toastr: ToastrService) {}

showToast(user: any): void {
    this.toastr.success('', 'Informations Utilisateur', {
      closeButton: true,
      timeOut: 5000, // Durée d'affichage du toast (en millisecondes)
      extendedTimeOut: 2000, // Temps supplémentaire d'affichage du toast après survol (en millisecondes)
      toastClass: 'ngx-toastr',
      titleClass: 'toast-title'
    });
  }

  connexion() {
    const userData = {
      email: this.email,
      mot_de_passe: this.motDePasse
    };

    this.backendService.connexion(userData).subscribe(
      (response) => {
        console.log(response);
        if (response && response.user) {
          this.backendService.showUserInfoToast(response.user);
        }
        // Rediriger l'utilisateur ou afficher un message de succès
      },
      (error) => {
        console.error(error);
        // Afficher un message d'erreur à l'utilisateur
      }
    );
  }
}
